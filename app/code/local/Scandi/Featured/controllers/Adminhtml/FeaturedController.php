<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Adminhtml_FeaturedController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout();

        return $this;
    }

    /**
     * Featured list grid action
     */
    public function indexAction()
    {
        $this->_initAction()->_addContent($this->getLayout()->createBlock('scandi_featured/adminhtml_featured'))->renderLayout();
    }

    /**
     * Add new featured list
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * Featured list edit action
     */
    public function editAction()
    {
        $model = Mage::getModel('scandi_featured/featured');
        $data = $this->_getSession()->getFormData(true);

        if ($id = $this->getRequest()->getParam('id', false)) {
            $model->load($id);

            if (!$model->getId()) {
                $this->_getSession()->addError($this->__('Featured does not exist'));
                $this->_redirect('*/*/');

                return;
            }
        }

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('featured_data', $model);

        $this->_initAction()->_addContent($this->getLayout()->createBlock('scandi_featured/adminhtml_featured_edit'))->_addLeft($this->getLayout()->createBlock('adminhtml/store_switcher'))->_addLeft($this->getLayout()->createBlock('scandi_featured/adminhtml_featured_edit_tabs'))->renderLayout();
    }

    /**
     *  Featured list save action
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('scandi_featured/featured');

            if ($id = $this->getRequest()->getParam('id', false)) {
                $model->load($id);

                if (!$model->getId()) {
                    $this->_getSession()->addError($this->__('This Featured list no longer exists.'));
                    $this->_redirect('*/*/index');

                    return;
                }
            }

            try {
                $model->addData($data);

                // Add product - featured list relation
                if (isset($data['featured_assigned_products'])) {
                    $productIds = Mage::helper('adminhtml/js')->decodeGridSerializedInput($data['featured_assigned_products']);
                    $featuredProductModel = Mage::getModel('scandi_featured/featured_product');
                    $featuredProductModel->addRelations($model, $productIds, array_reverse($data['sort_order']));

                }

                $model->save();
                $this->_getSession()->addSuccess($this->__('Featured list was successfully saved.'));

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'store' => $model->getStoreId()));

                    return;
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                Mage::logException($e);
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Featured list delete action
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('scandi_featured/featured');
                $model->load($id);
                $model->delete();
                $this->_getSession()->addSuccess($this->__('The featured list has been deleted.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));

                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find the productpromotion to delete.'));
        $this->_redirect('*/*/');
    }

    /**
     * Featured list mass delete action
     */
    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids', false);
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select item(s)'));
        } else {
            try {
                $collection = Mage::getResourceModel('scandi_featured/featured_collection');
                $collection->addFieldToFilter('featured_id', $ids);
                $collection->walk('delete');

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);

                return;
            }
        }

        $this->_redirect('*/*/index');
    }

    /**
     * Add current featured model to register
     *
     * @return bool
     */
    protected function _initFeatured()
    {
        $model = Mage::getModel('scandi_featured/featured');
        if (($id = $this->getRequest()->getParam('id'))) {
            $model->load($id);
            if (!$model->getId()) {
                return false;
            }
        }
        Mage::register('featured_data', $model);

        return $model;
    }

    /**
     * Assigned products (with serializer block)
     */
    public function productsAction()
    {
        $this->_title($this->__('Featured'))->_title($this->__('Fetured Products'));
        $this->_initFeatured();
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Assigned products grid
     */
    public function productsgridAction()
    {
        $this->_initFeatured();
        $this->loadLayout();
        $this->renderLayout();
    }
}