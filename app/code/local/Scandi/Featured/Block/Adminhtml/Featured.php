<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Block_Adminhtml_Featured extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Scandi_Featured_Block_Adminhtml_Featured constructor.
     */
    public function __construct()
    {
        $this->_blockGroup = 'scandi_featured';
        $this->_controller = 'adminhtml_featured';
        $this->_headerText = $this->__('Featured product list');
        $this->_addButtonLabel = $this->__('New featured product list');
        parent::__construct();
    }
}