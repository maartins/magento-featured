<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Block_Adminhtml_Featured_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Scandi_Featured_Block_Adminhtml_Featured_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('featured_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Featured list'));
    }

    /**
     * Add tabs
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
            'label' => $this->__('Product for promotion'),
            'content' => $this->getLayout()->createBlock('scandi_featured/adminhtml_featured_edit_tab_info')->initForm()->toHtml(),
        ));

        if (Mage::registry('featured_data') && Mage::registry('featured_data')->getId()) {
            $this->addTab('products', array(
                'label' => $this->__('Products'),
                'class' => 'ajax',
                'url' => $this->getUrl('*/*/products', array('_current' => true)),
            ));
        }

        return parent::_beforeToHtml();
    }
}
