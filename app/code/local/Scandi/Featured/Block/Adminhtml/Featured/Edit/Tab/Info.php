<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Block_Adminhtml_Featured_Edit_Tab_Info extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Getter for current model
     *
     * @return mixed
     */
    protected function _getModel()
    {
        return Mage::registry('featured_data');
    }

    /**
     * @return $this
     */
    public function initForm()
    {
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('scandi_form',
            array('legend' => $this->__('Featured info')));

        $fieldset->addField('title', 'text', array(
            'label' => $this->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
        ));

        $fieldset->addField('description', 'editor', array(
            'name' => 'description',
            'label' => $this->__('Description'),
            'title' => $this->__('Description'),
            'required' => true,
            'style' => 'height:4em',
        ));

        if (Mage::getSingleton('adminhtml/session')->getFeaturedData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getFeaturednData());
            $model = Mage::getModel('scandi_featured/featured');
            $model->setData(Mage::getSingleton('adminhtml/session')->getFeaturedData());
            $form->setDataObject($model);
            Mage::getSingleton('adminhtml/session')->setFeaturedData(null);
        } elseif ($this->_getModel()) {
            $form->setValues($this->_getModel()->getData());
            $form->setDataObject($this->_getModel());
        }

        $this->setForm($form);

        return $this;
    }
}
