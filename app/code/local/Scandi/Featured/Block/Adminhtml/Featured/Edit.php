<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Block_Adminhtml_Featured_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Scandi_Featured_Block_Adminhtml_Featured_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'scandi_featured';
        $this->_controller = 'adminhtml_featured';
        $this->_updateButton('save', 'label', $this->__('Save featured list'));
        $this->_updateButton('delete', 'label', $this->__('Delete featured list'));
        $this->_addButton('saveandcontinue', array(
            'label' => $this->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('featured_data') && Mage::registry('featured_data')->getId()) {
            return $this->__('Edit featured line - "%s"',
                Mage::registry('featured_data')->getTitle());
        } else {
            return $this->__('New featured product list');
        }
    }
}