<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Block_Adminhtml_Featured_Edit_Tab_Products extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * @var $_currentModel
     */
    protected $_currentModel;

    /**
     * Scandi_Featured_Block_Adminhtml_Featured_Edit_Tab_Products constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->_currentModel = Mage::registry('featured_data');
        $this->setId('productsGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setDefaultFilter(array('in_featured' => 1));
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(false);
    }

    /**
     * Get Featured ID
     *
     * @return int
     */
    protected function _getFeaturedId()
    {
        return $this->_currentModel->getId();
    }

    /**
     * Add filter to grid columns
     *
     * @param mixed $column
     *
     * @return Scandi_Featured_Block_Adminhtml_Productpromotion_Edit_Tab_Products
     */
    protected function _addColumnFilterToCollection($column)
    {

        if ($column->getId() == 'in_featured') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * Retrieve Products Collection
     *
     * @return Scandi_Featured_Block_Adminhtml_Productpromotion_Edit_Tab_Products
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('attribute_set_id')
            ->addAttributeToSelect('category_id');

        $collection->getSelect()->joinLeft(
            array('product' => $collection->getTable('scandi_featured_featured_product')),
            'e.entity_id = product.product_id AND product.featured_id =' . $this->_currentModel->getId(),
            array('sort_order'));

        $this->setCollection($collection);

        parent::_prepareCollection();
        $this->getCollection()->addWebsiteNamesToResult();

        return $this;
    }

    /**
     * Prepeare columns for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('in_featured', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'field_name' => 'in_featured',
            'values' => $this->_getSelectedProducts(),
            'align' => 'center',
            'index' => 'entity_id',
        ));

        $this->addColumn('entity_id',
            array(
                'header' => $this->__('ID'),
                'width' => 50,
                'type' => 'number',
                'index' => 'entity_id',
                'filter' => false,
                'sortable' => false,
            ));

        $this->addColumn('sku',
            array(
                'header' => $this->__('SKU'),
                'width' => 80,
                'index' => 'sku',
                'filter' => false,
                'sortable' => false,
            ));

        $this->addColumn('name',
            array(
                'header' => $this->__('Name'),
                'index' => 'name',
                'filter' => false,
                'sortable' => false,
            ));

        $this->addColumn('sort_order[]',
            array(
                'header' => $this->__('Order'),
                'type' => 'input',
                'index' => 'sort_order',
                'filter' => false,
                'sortable' => false,
            ));

        return parent::_prepareColumns();
    }

    /**
     * Retrieve related products
     *
     * @return array
     */
    protected function _getSelectedProducts()
    {
        $products = $this->getRequest()->getPost('assigned_products', null);
        if (!is_array($products)) {
            $products = $this->getFeaturedProducts();
        }

        return $products;
    }

    /**
     * Retrieve Grid Url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getData('grid_url')
            ? $this->getData('grid_url')
            : $this->getUrl('*/*/productsgrid', array('_current' => true));
    }

    /**
     * Retrieve featured products
     *
     * @return array
     */
    public function getFeaturedProducts()
    {
        return $this->_currentModel->getFeaturedProductIds();
    }
}
