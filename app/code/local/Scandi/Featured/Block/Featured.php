<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Block_Featured extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    public $featuredId;
    protected $_priceTemplate = 'catalog/product/price.phtml';
    protected $_template = 'scandi/featured/list.phtml';

    /**
     * Scandi_Featuredproducts_Block_Widget constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->featuredId = $this->getData('featured_list');
        if ($this->getData('price_template')) {
            $this->_priceTemplate = $this->getData('price_template');
        }
    }

    /**
     * @return Scandi_Featured_Model_Resource_Featured_Collection
     * @throws Mage_Core_Exception
     */
    public function getFeatured()
    {
        if ($this->featuredId) {
            $_featured = Mage::getResourceModel('scandi_featured/featured_collection')
                ->addFieldToFilter('featured_id', $this->featuredId)
                ->setPageSize(1)
                ->setCurPage(1)
                ->getFirstItem();

            return $_featured;
        }
    }

    /**
     * @return Scandi_Featuredproducts_Model_Featured
     */
    protected function _getModel()
    {
        return Mage::getSingleton('scandi_featuredproducts/featured');
    }

    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProducts()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addUrlRewrite()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('small_image')
            ->addAttributeToFilter('entity_id', array('in' => $this->_getLoadedProductArray()))
            ->joinTable(
                array('feaatured_product' => 'scandi_featured_featured_product'), 'product_id=entity_id',
                array('sort_order' => 'sort_order')
            )
            ->setOrder('sort_order', 'DESC');

        return $collection;
    }

    /**
     * @param $product
     *
     * @return string
     */
    public function getPriceHtml($product)
    {
        $this->setTemplate($this->_priceTemplate);
        $this->setProduct($product);

        return $this->toHtml();
    }

    /**
     * @return array
     */
    protected function _getLoadedProductArray()
    {
        $productIdArray = array();
        if ($this->featuredId) {
            $_productCollection = Mage::getModel('scandi_featured/featured_product')->getCollection();
            $_productCollection->addFieldToFilter('featured_id', array('eq' => $this->featuredId))
                ->setOrder('sort_order', 'DESC');

            if ($_productCollection->getSize()) {
                foreach ($_productCollection as $product) {
                    if ($product->getFeaturedId() && $product->getProductId()) {
                        $productIdArray[] = $product->getProductId();
                    }
                }
            }
        }

        return $productIdArray;
    }
}