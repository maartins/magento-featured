Scandi_Featured
==================

This module is used to create custom featured lists with product relation, to make custom product list output in FE.
Module can display featured list as slider in frontend using widgets or block. For widget it is possible to specify the list to display and in block list with biggest position number will be displayed.
Module have Mage_Catalog dependency to get product attributes.
It has two tables:
    scandi_featured_featured - for storing featured list related things
    scandi_featured_featured_product table which is connected with scandi_featured_featured table featured_id for storing relations with products and custom sort order.
Module can store translations for store view level.


