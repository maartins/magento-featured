<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Model_Featured extends Mage_Catalog_Model_Abstract
{
    protected $_relatedProductIds;

    function _construct()
    {
        parent::_construct();
        $this->_init('scandi_featured/featured');
    }

    /**
     * Retrieves array of featured product ids
     *
     * @return array
     */
    public function getFeaturedProductIds()
    {
        return Mage::getModel('scandi_featured/featured_product')->setFeaturedId($this->getId())->getProductIds();
    }
}
