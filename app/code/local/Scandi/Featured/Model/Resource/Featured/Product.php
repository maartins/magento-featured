<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Model_Resource_Featured_Product extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initialize resource connection and define table resource
     *
     */
    protected function _construct()
    {
        $this->_init('scandi_featured/featured_product', 'value_id');
    }

    /**
     * Retrieve featured Products
     *
     * @param Scandi_Featured_Model_Resource_Featured_Product $model
     *
     * @return array
     */
    public function getProductIds($model)
    {
        $select = $this->_getReadAdapter()->select()
            ->from($this->getMainTable(), 'product_id')
            ->where("{$this->getMainTable()}.featured_id=?", $model->getFeaturedId());

        if (!is_null($model->getStatusFilter())) {
            $select->join($this->getTable('scandi_featured/featured'),
                "{$this->getTable('scandi_featured/featured')}.entity_id = {$this->getMainTable()}.featured_id")
                ->where("{$this->getTable('scandi_featured/featured')}.status = ?", $model->getStatusFilter());
        }

        return $this->_getReadAdapter()->fetchCol($select);
    }

    /**
     * Add Line to Product relations
     *
     * @param Scandi_Featured_Model_Resource_Featured_Product $model
     *
     * @return $this
     */
    public function addRelations($model)
    {
        $addedIds = $model->getAddedProductIds();
        $orders = $model->getAddedSortOrders();
        $orders = array_values($orders);

        $insertData = array();

        Mage::getResourceModel('scandi_featured/featured_product_collection')
            ->addFieldToFilter('featured_id', $model->getFeaturedId())
            ->walk('delete');

        foreach ($addedIds as $index => $value) {
            $insertData[] = array(
                'featured_id' => $model->getFeaturedId(),
                'product_id' => $value,
                'sort_order' => (!empty($orders)) ? $orders[$index] : null,
            );
        }
        $this->_getWriteAdapter()->insertMultiple($this->getMainTable(), $insertData);

        return $this;
    }
}
