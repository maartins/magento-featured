<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Model_Resource_Featured_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     *  Scandi_Featured_Model_Resource_Featured_Collection construct
     */
    protected function _construct()
    {
        $this->_init('scandi_featured/featured');
    }
}
