<?php

/**
 * @category Scandi
 * @package Scandi\Featured
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Model_Featured_Product extends Mage_Core_Model_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('scandi_featured/featured_product');
    }

    /**
     * Retrieve Relation Product Ids
     *
     * @return array
     */
    public function getProductIds()
    {
        $ids = $this->getData('product_ids');
        if (is_null($ids)) {
            $ids = $this->_getResource()->getProductIds($this);
            $this->setProductIds($ids);
        }

        return $ids;
    }

    /**
     * Retrieve list of related Featured ids for products specified in current object
     *
     * @return array
     */
    public function getRelatedFeaturedIds()
    {
        if (is_null($this->getData('related_featured_ids'))) {
            $this->setRelatedFeaturedIds($this->_getResource()->getFeaturedIds($this));
        }

        return $this->getData('related_featured_ids');
    }

    /**
     * Add Featured to Product relations
     *
     * @param Scandi_Featured_Model_Featured $model
     * @param array
     *
     * @return Scandi_Featured_Model_Featured
     */
    public function addRelations(Scandi_Featured_Model_Featured $model, $productIds = array(), $sort)
    {
        $this->setAddedProductIds($productIds);
        $this->setFeaturedId($model->getId());
        $this->setAddedSortOrders(array_filter($sort));
        $this->_getResource()->addRelations($this);

        return $this;
    }
}
