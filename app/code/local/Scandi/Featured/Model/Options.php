<?php

/**
 * @category Scandi
 * @package Scandi\Featuredproducts
 * @author Martins Saukums <info@scandiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
class Scandi_Featured_Model_Options extends Mage_Core_Model_Abstract
{
    /**
     * Provide available options as a value/label array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        $featuredCollection = Mage::getResourceModel('scandi_featured/featured_collection')
            ->addFieldToSelect('featured_id')
            ->addFieldToSelect('title');

        foreach ($featuredCollection as $featured) {
            if ($featured->getId()) {
                $result[] = array(
                    'value' => $featured->getId(),
                    'label' => $featured->getTitle(),
                );
            }
        }

        return $result;
    }
}