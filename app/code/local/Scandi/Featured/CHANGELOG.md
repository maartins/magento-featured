Scand_Featured
==================

0.1.0:
- initial module creation
- added DB table structure
- added blocks for display featured list that will display featured list biggest position
- added widget block and functionality to create featured widget and specify specific list
- added models/collections for product and featured
- added empty FE layout with template for display featured product carousel
- added initial output for featured list grid and form
- added slick slider
- added admin edit tabs for grid to select featured products
- added functionality to check items for featured list and ability to set display order inf FE

